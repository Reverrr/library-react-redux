//Vendors
import React, { ReactElement } from 'react';
import { createStore, combineReducers, AnyAction, ReducersMapObject } from 'redux';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
//Components
import App from './views/app/app';
import { ErrorBoundry } from './views/error-boundry/error-boundry'
import { BookStoreServiceProvider } from './views/book-store-service-context/book-store-service-context';
//Services
import BookStoreService from './services/book-store.service';
import UserStoreService from './services/user-store.service';
import { UserStoreServiceProvider } from './views/user-store-service-context/user-store-service-context';
//Actions
import { userReducer, UsersStoreState } from './stores/users';
import bookReducer from './stores/books/book.reducer';
import { BooksStoreState } from './stores/books/book.reducer';

export interface ApplicationState {
    user: UsersStoreState,
    book: BooksStoreState
}

export const reducers = {
    user: userReducer,
    book: bookReducer
};

const store = createStore(combineReducers(reducers));

const bookStoreService: BookStoreService = new BookStoreService();
const userStoreService: UserStoreService = new UserStoreService();

const update = () => {
    ReactDOM.render(
        <Provider store={store}>
            <ErrorBoundry>
                <BookStoreServiceProvider value={bookStoreService}>
                    <UserStoreServiceProvider value={userStoreService}>
                        <Router>
                            <Route path='/' render={(): ReactElement => {
                                return (
                                    <App />
                                );
                            }} />
                        </Router>
                    </UserStoreServiceProvider>
                </BookStoreServiceProvider>
            </ErrorBoundry>
        </Provider>
        , document.getElementById('root'));
};

store.subscribe(() => {
    update();
});

update();