//Vendors
import { createStore, combineReducers } from 'redux';

import { userReducer } from './stores/users';
import bookReducer from './stores/books/book.reducer';

export const reducers = {
    users: userReducer,
    books: bookReducer
};

const store = createStore(combineReducers(reducers));

export default store;