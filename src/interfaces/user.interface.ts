import { UserErrorInterface } from './user-error.interface';

export interface UserInterface {
    username: string;
    firstName: string;
    lastName: string;
    role: string;
    password: string;
    _id?: string;
    token?: string;
    errors?: UserErrorInterface;
    userAlreadyExist?: boolean;
}