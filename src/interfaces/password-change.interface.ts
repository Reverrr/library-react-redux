//Interfaces
import { UserInterface } from './user.interface';

export interface PasswordInterface {
    userModel: UserInterface;
    currentPassword: string;
    newPassword: string;
    error?: boolean;
}