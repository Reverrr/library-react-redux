//Interfaces
import { UserInterface } from './user.interface';

export interface UserAccessInterface {
    userModel: UserInterface;
    accessToken: string;
    newToken: string;
}