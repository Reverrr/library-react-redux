export interface CountInterface {
    totalCountUsers: number;
    totalCountBooks: number;
    id?: string;
}