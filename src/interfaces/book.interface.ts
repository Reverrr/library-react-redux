//Interfaces
import { AuthorInterface } from './author.interface';

export interface BookInterface {
    name: string;
    img: string;
    description: string;
    type: string;
    author: AuthorInterface[];
    _id?: string;
}