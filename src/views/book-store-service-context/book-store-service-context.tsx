//Vendors
import React from 'react';



let defaultValue: any;

const {
    Provider: BookStoreServiceProvider,
    Consumer:BookStoreServiceConsumer
} = React.createContext(defaultValue);


export {
    BookStoreServiceProvider,
    BookStoreServiceConsumer
};