//Vendors
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
//Interfaces
import { UserInterface, UserErrorInterface } from '../../interfaces';
//Components
import ChangePassword from '../change-password/change-password';
//Services
import UserStoreService from '../../services/user-store.service';
import wrapUserStoreService from "../hoc/wrap-user-store.service";
//Actions
import { DispatchMapper, DispatchProps } from '../../stores/users/user.action';
import { ApplicationState } from '../../index';
import { UsersStoreState } from "../../stores/users";
//Style
import './profile.scss';

interface CreateUserPropsInterface {
    userStoreService: UserStoreService;
    user: UserInterface;
    match: {
        params: {
            id: string;
        }
    }
}

type State = {
    _id: string,
    username: string,
    firstName: string,
    lastName: string,
    role: string,
    isPasswordChange: boolean,
    errors: {
        username: boolean,
        lastName: boolean,
        firstName: boolean,
    },
    isPasswordChangeSuccessfully: boolean,
    userAlreadyExist: boolean
}

type Props = RouteComponentProps<{}> & CreateUserPropsInterface & DispatchProps & UsersStoreState;

class Profile extends Component<Props> {

    public state: State = {
        _id: '',
        username: '',
        firstName: '',
        lastName: '',
        role: 'user',
        isPasswordChange: false,
        errors: {
            username: false,
            lastName: false,
            firstName: false,
        },
        isPasswordChangeSuccessfully: false,
        userAlreadyExist: false
    };

    constructor(props: Props) {
        super(props);
    }

    public componentDidMount() {
        this.userLoaded();
    }
    private userLoaded() {
        const props = this.props;
        props.userStoreService.getUser(props.match.params.id)
            .then(
                (data: UserInterface) => {
                    this.setState({
                        _id: data._id,
                        username: data.username,
                        firstName: data.firstName,
                        lastName: data.lastName,
                        role: data.role
                    });
                    props.userLoaded(data);
                }
            );
    }

    public isPasswordEditupdateData = (value: boolean, status: boolean = false) => {
        this.setState({
            isPasswordChange: value,
            isPasswordChangeSuccessfully: status
        })
    }

    public render() {
        const props = this.props;
        const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
            const name = event.target.name;
            this.setState({
                [name]: event.target.value
            });
        }

        const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            /**
             * VALIDATION
             */
            const regexForUsername = /^[a-zA-Z0-9]{4,16}$/g;
            const regexForFirstName = /^[a-zA-Z]{2,14}$/g;
            const regexForLastName = /^[a-zA-Z]{2,14}$/g;
            const resultRegexForUsername = regexForUsername.test(this.state.username);
            const resultregexForFirstName = regexForFirstName.test(this.state.firstName);
            const resultregexForLastName = regexForLastName.test(this.state.lastName);
            let errors: UserErrorInterface = Object.assign({}, this.state.errors);
            if (!resultRegexForUsername) {
                errors.username = true;
                this.setState({ errors });
            } else {
                errors.username = false;
                this.setState({ errors });
            }
            if (!resultregexForFirstName) {
                errors.firstName = true;
                this.setState({ errors });
            } else {
                errors.firstName = false;
                this.setState({ errors });
            }
            if (!resultregexForLastName) {
                errors.lastName = true;
                this.setState({ errors });
            } else {
                errors.lastName = false;
                this.setState({ errors });
            }
            if ((resultRegexForUsername
                && resultregexForFirstName
                && resultregexForLastName)) {
                    const createUser: Object = {
                        username: this.state.username,
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        role: this.state.role
                    }
                props.userStoreService.changeUser(props.match.params.id, createUser as any)
                    .then(
                        (data: any) => {
                            if (data.statusCode !== 500) {
                                const props = this.props;
                                props.userLoaded((this.state as any));
                                if (!!props.history) {
                                    props.history.goBack();
                                }
                            } else {
                                this.setState({
                                    userAlreadyExist: true
                                });
                            }
                        }
                    );
            }
        }
        return (
            <Fragment>
                {(!!this.state) ? (
                    <div className="container flex-column">
                        <h2>It's your profile&nbsp;
                    <span className="profile-data">{(props.user) ? (props.user.firstName) : null}&nbsp;
                    {(props.user) ? (props.user.lastName): null }</span>!!</h2>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">Username: </label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="username" type="text" value={this.state.username} onChange={handleChange} />
                                    {(this.state.errors.username) ? (
                                        <span className="error">Username is required and must have min 4, max 16 characters</span>
                                    ) : null}
                                    {(this.state.userAlreadyExist) ? (<div className="error">User already exist</div>) : null}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">First Name:</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="firstName" type="text" value={this.state.firstName} onChange={handleChange} />
                                    {(this.state.errors.firstName) ? (
                                        <span className="error">First Name is required and must have min 2, max 14 latin alphabet characters</span>
                                    ) : null}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">Last Name:</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="lastName" type="text" value={this.state.lastName} onChange={handleChange} />
                                    {(this.state.errors.lastName) ? (
                                        <span className="error">Last Name is required and must have min 2, max 14 latin alphabet characters</span>
                                    ) : null}
                                </div>
                            </div>
                            <div className="btn-send">
                                <input className="btn btn-primary mt-2 w-25" type="submit" value="Submit" />
                            </div>
                        </form>
                        <div>
                            <button className="btn btn-outline-dark" onClick={() => {
                                this.setState({
                                    isPasswordChange: !this.state.isPasswordChange,
                                    isPasswordChangeSuccessfully: false
                                });
                            }}>Edit password</button>
                            {(this.state.isPasswordChange) ? (
                                <ChangePassword props={props} isPasswordEditupdateData={this.isPasswordEditupdateData} userId={this.state._id} />
                            ) : null}
                            {(this.state.isPasswordChangeSuccessfully
                            ) ? (<span className="text-success ml-2">Password successfully changed</span>) : null}
                        </div>
                    </div>
                ) : (<div>Wait plz</div>)}
            </Fragment>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {
        currentUser: state.user.currentUser,
        user: state.user.currentUser
    }
}

export default wrapUserStoreService()(connect(mapStateToProps, DispatchMapper)(Profile));