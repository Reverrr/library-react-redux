//Vendors
import React from "react";
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';
//Interfaces
import { UserInterface } from '../../interfaces/user.interface';
//Styles
import './header.scss';

interface HeaderPropsInterface {
    props: {
        user: UserInterface
    };
    history: {
        go(value: string): void;
    }
}

type Props = RouteComponentProps<{}> & HeaderPropsInterface;

const Header = (props: Props) => {
    return (
        <nav className="navbar-header navbar navbar-expand navbar-dark bg-dark">
            <div className="navbar-nav">
                <Link to="/" className="nav-item nav-link">Home</Link>
                {(props.props.user.role === 'admin') ? (
                    <Link to="/admin/users" className="nav-item nav-link" >Admin</Link>
                ) : null}
                <Link to={`/profile/${props.props.user._id}`} className="nav-item nav-link">Profile</Link>
            </div>
            <div className="user-info navbar-nav">
                <Link to={`/profile/${props.props.user._id}`} className="nav-item nav-link">HI! {props.props.user.username}</Link>
                <button onClick={() => {
                    localStorage.clear();
                    props.history.go('/login');
                }} className="nav-item nav-link btn btn-danger">Logout</button>
            </div>
        </nav>
    );
}
export default withRouter(Header);
