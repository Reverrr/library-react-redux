//Vendors
import React, { Component } from "react";
import { RouteComponentProps } from "react-router-dom";
//Services
import UserStoreService from "../../services/user-store.service";
import wrapUserStoreService from "../hoc/wrap-user-store.service";
//Interfaces
import { UserInterface, PasswordInterface } from '../../interfaces';
//Actions
import { DispatchProps } from "../../stores/users";
//Styles
import 'react-picky/dist/picky.css';

interface CreateUserPropsInterface {
    userStoreService: UserStoreService;
    match: {
        params: {
            id: string;
        }
    }
    userId: string;
    isPasswordEditupdateData(value: boolean, status: boolean): void;
}

type Props = RouteComponentProps<{}> & CreateUserPropsInterface & DispatchProps;

class ChangePassword extends Component<Props> {
    public _isMounted = false;
    public state: PasswordInterface = {
        userModel: {} as UserInterface,
        currentPassword: '',
        newPassword: '',
        error: false
    };

    public componentDidMount() {
        const props = this.props
        this._isMounted = true;
        props.userStoreService.getUser(props.userId)
            .then(
                (data: UserInterface) => {
                    this.setState({
                        userModel: {
                            username: data.username,
                            firstName: data.firstName,
                            lastName: data.lastName,
                            role: data.role,
                            password: data.password
                        }
                    });
                }
            );
    }

    public componentWillUnmount() {
        this._isMounted = false;
    }

    public render() {
        const props = this.props;
        const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
            const name = event.target.name;
            this.setState({
                [name]: event.target.value
            });
        }
        const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            props.userStoreService.changePassword(this.state)
                .then((data: any) => {
                    if (this._isMounted) {
                        if (data.status === 201) {
                            props.isPasswordEditupdateData(false, true);
                            if (this._isMounted) {
                                this.setState({
                                    error: false
                                });
                            }
                        } else {
                            if (this._isMounted) {
                                this.setState({
                                    error: true,

                                });
                            }
                        }
                    }
                })
        }
        return (
            <form className="mt-3" onSubmit={handleSubmit}>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label col-form-label-sm">Current password: </label>
                    <div className="col-sm-10">
                        <input className="form-control" name="currentPassword" type="password" onChange={handleChange} />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label col-form-label-sm">New password: </label>
                    <div className="col-sm-10">
                        <input className="form-control" name="newPassword" type="password" onChange={handleChange} />
                    </div>
                </div>
                {(this.state.error) ? (<span className="error">Incorrect password</span>) : null}
                <div className="row justify-content-md-center">
                    <input className="btn btn-primary mt-2 w-75" type="submit" value="Change password" />
                </div>
            </form>
        );
    }
}

export default wrapUserStoreService()(ChangePassword);