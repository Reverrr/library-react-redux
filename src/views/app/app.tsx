//Vendors
import React, { Component } from "react";
import { Route, Switch, Redirect, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
//Services
import wrapUserStoreService from "../hoc/wrap-user-store.service";
import AuthStoreService from '../../services/auth.service';
//Components
import HomePage from "../pages/home-page";
import BookSingleItem from '../book-single-item/book-single-item';
import Admin from "../admin/admin";
import Header from '../header/header';
import Login from '../pages/login/login'
import Register from '../pages/register/register';
import Profile from '../profile/profile';
//Interfaces
import { UserInterface, UserAccessInterface } from '../../interfaces';
//Actions
import { DispatchMapper, DispatchProps } from '../../stores/users/user.action';
import { ApplicationState } from '../../index';

interface AppPropsInterface {
    authStoreService: AuthStoreService;
    user: UserInterface;
    props: RouteComponentProps<{}>;
    changeUser: UserInterface;
    match: {
        params: {
            id: string;
        }
    }
}

type State = {
    user: UserInterface,
    token: string
}

type Props = RouteComponentProps<{}> & AppPropsInterface & DispatchProps;

class App extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            user: {
                _id: '',
                username: '',
                firstName: '',
                lastName: '',
                role: '',
                password: '',
            },
            token: '',
        }
    }

    public componentDidMount() {
        const props = this.props;
        let token = localStorage.getItem('currentUser');
        if (token) {
            props.authStoreService.auth(token)
                .then((data: UserAccessInterface) => {
                    localStorage.setItem('currentUser', data.newToken);
                    this.setState({
                        user: data.userModel,
                    })
                    props.currentUser(data.userModel);
                    return data;
                });
        }
    }
    public render() {
        return (
            <div>
                {(!!this.props.user && (this.props.user as any) !== 401)
                    ? (
                        <div>
                            <Header props={(this.props.user) ? { user: this.props.user } : this.state} />
                            <Switch>
                                <Route path='/' component={HomePage} exact />
                                <Route path='/books/:id' component={BookSingleItem} exact />
                                <Route path='/profile/:id' component={Profile} exact />
                                {(!!((this.props.changeUser) ? { user: this.props.user } : this.state).user && ((this.props.user) ? { user: this.props.user } : this.state).user.role === 'admin') ? (
                                    <Route path='/admin' component={Admin} />
                                ) : (
                                        <Redirect from='/' to='/' />
                                    )
                                }
                                <Redirect from='/' to='/' />
                            </Switch>
                        </div>
                    ) : (
                        <Switch>
                            <Route path='/register' component={Register} exact />
                            <Route exact path='/login' render={(props) => {
                                return (
                                    <Login props={props} />
                                );
                            }} />
                            <Redirect from='/' to='/login' />
                        </Switch>
                    )
                }
            </div>
        );
    }
}
const mapStateToProps = (state: ApplicationState) => {
    return {
        user: state.user.currentUser,
        token: state.user.token
    }
}

export default wrapUserStoreService()(connect(mapStateToProps, DispatchMapper)(App));
