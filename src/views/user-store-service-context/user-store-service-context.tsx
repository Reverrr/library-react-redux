//Vendors
import React from 'react';



let defaultValue: any;

const {
    Provider: UserStoreServiceProvider,
    Consumer:UserStoreServiceConsumer
} = React.createContext(defaultValue);


export {
    UserStoreServiceProvider,
    UserStoreServiceConsumer
};