//Vendors
import React, { Fragment, Component } from "react";
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
//Services
import wrapBookStoreService from "../hoc/wrap-book-store.service";
import BookStoreService from "../../services/book-store.service";
//Interfaces
import { BookInterface, AuthorInterface, UserInterface } from '../../interfaces';
//Actions
import { DispatchMapper, DispatchProps } from '../../stores/books/book.action';
import { BooksStoreState } from '../../stores/books/book.reducer';
import { ApplicationState } from '../../index';

interface BookListPropsInterface {
    bookStoreService: BookStoreService,
    currentUser: UserInterface,
    match: {
        params: {
            id: string;
        }
    };
    history: History;

}

export type Props = RouteComponentProps<{}> & BooksStoreState & DispatchProps & BookListPropsInterface;

class BookSingleItem extends Component<Props> {
    public componentDidMount() {
        const props = this.props;
        props.bookStoreService.getBookById(props.match.params.id)
            .then(
                (body: BookInterface) => {
                    props.bookLoaded(body)
                }
            )
    }
    public render() {
        const props = this.props;
        const error = (
            <div>Wait plz</div>
        );
        return (
            <Fragment>
                <div className="container d-flex flex-column align-items-center">
                    {(!!this.props.book && this.props.book.name) ? (
                        <div className="book-item">
                            <h3>Title: {this.props.book.name}</h3>
                            <img src={this.props.book.img} />
                            <div>
                                <span className="reduce-text">Authors: {this.props.book.author.map((item: AuthorInterface, index: number) => {
                                    return (<span className="reduce-text" key={item._id}>{(index + 1 < this.props.book.author.length) ? (`${item.name}, `) : (item.name)} </span>)
                                })}</span>
                            </div>
                            <span>Description: {this.props.book.description}</span>
                        </div>
                    ) : error}
                    <div className="d-flex">
                        <button className="btn btn-secondary mr-2" onClick={() => {
                            props.history.goBack();
                        }}>Back</button>
                        {(props.currentUser.role === 'admin')
                            ? (
                                <Link className="btn btn-primary" to={`/admin/books/edit-book/${props.match.params.id}`}>Edit Book</Link>
                            ) : null}
                    </div>
                </div>
            </Fragment>
        );
    }
}
const mapStateToProps = (state: ApplicationState) => {
    return {
        book: state.book.book,
        currentUser: state.user.currentUser
    }
}

export default wrapBookStoreService()(connect(mapStateToProps, DispatchMapper)(BookSingleItem));
