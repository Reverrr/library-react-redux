//Vendors
import React, { Component } from 'react'
import ErrorIndicator from '../error-indicator/error-indicator';

export class ErrorBoundry extends Component {

    public state = {
        hasError: false
    };

    public componentDidCatch() {
        this.setState({ hasError: true });
    }

    public render() {
        if (this.state.hasError) {
            return <ErrorIndicator />
        }
        return this.props.children;
    }
}