//Vendors
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, RouteComponentProps } from 'react-router-dom';
//Services
import wrapUserStoreService from "../../hoc/wrap-user-store.service";
import AuthStoreService from '../../../services/auth.service';
//Actions
import { DispatchMapper, DispatchProps } from "../../../stores/users/user.action";
import { ApplicationState } from '../../../index';

interface BookListPropsInterface {
    authStoreService: AuthStoreService,
    match: {
        params: {
            id: string;
        }
    };
    props: {
        history: {
            go(value: string): void;
        };
    }

}

type State = {
    username: string,
    password: string,
    error: boolean
}

export type Props = RouteComponentProps<{}> & DispatchProps & BookListPropsInterface;

class Login extends Component<Props> {

    public state: State = {
        username: '',
        password: '',
        error: false
    };

    public componentDidMount() { }

    public render() {
        const props = this.props;
        const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
            const name = event.target.name;
            this.setState({
                [name]: event.target.value
            });
        }

        const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            props.authStoreService.login(this.state)
                .then(
                    (data: any) => {
                        const props = this.props;
                        if (!!data && data.accessToken) {
                            props.setToken(data.accessToken);
                            props.props.history.go('/');
                            localStorage.setItem('currentUser', data.accessToken);
                            this.setState({
                                error: false
                            })
                        } else {
                            this.setState({
                                error: true
                            })
                        }
                    },
                    (error: any) => {
                        console.log(error);
                    }
                );
        }
        return (
            <div>
                {(!!this.state) ? (
                    <div className="container flex-column">
                        <h2>Sign in to Library</h2>
                        <form className="form-inline" onSubmit={handleSubmit}>
                            <div className="form-group mb-2">
                                <label className="sr-only">Username: </label>
                                <input name="username" className="form-control" type="text" value={this.state.username} onChange={handleChange} />
                            </div>
                            <div className="form-group mx-sm-3 mb-2">
                                <label className="sr-only">Password:</label>
                                <input className="form-control" name="password" type="password" value={this.state.password} onChange={handleChange} />
                                <button className="btn btn-primary mx-sm-3 " type="submit">Sign in</button>
                            </div>
                        </form>
                        {(this.state.error) ? (<span className="error">Incorrect login or password</span>) : null}
                        <Link className="w-25" to="/register">Sign up</Link>
                    </div>
                ) : (<div>Wait plz</div>)}
            </div>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {};
}

export default wrapUserStoreService()(connect(mapStateToProps, DispatchMapper)(Login));