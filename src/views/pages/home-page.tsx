//Vendors
import React from 'react'
import MainLibrary from '../book-list/book-list';

const HomePage = () => {
    return (<div>
        <MainLibrary />
    </div>)
}

export default HomePage;