//Vendors
import React, { Fragment } from "react";
import { Link, RouteComponentProps } from "react-router-dom";
//Interfaces
import { BookInterface, AuthorInterface } from "../../interfaces";
//Styles
import './book-list-item.scss';

type BookListItemProps = {
    book: BookInterface
}

type Props = RouteComponentProps<{}> & BookListItemProps;

const BookListItem = (book: Props | any) => {
    const books: BookInterface = book.book;
    return (
        <Fragment>
            {(books) ? (<div className="book-item">
                <Link to={'books/' + books._id}>
                    <span className="title">{books.name}</span>
                    <img src={books.img} />
                </Link>
                <span className="reduce-text">Authors: {books.author.map((item: AuthorInterface) => {
                    return (<span key={item._id}>{item.name}</span>)
                })}</span>
                <span className="reduce-text">Description: {books.description}</span>
            </div>) : (<div>Plz Pait</div>)}
        </Fragment>
    );
}
export default BookListItem;