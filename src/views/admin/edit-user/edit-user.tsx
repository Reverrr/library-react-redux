//Vendors
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from 'react-router-dom';
//Services
import UserStoreService from '../../../services/user-store.service';
import wrapUserStoreService from "../../hoc/wrap-user-store.service";
//Interfaces
import { UserInterface, UserErrorInterface } from '../../../interfaces';
import { DispatchMapper } from "../../../stores/users";
import { ApplicationState } from "../../..";
//Styles
import './edit-user.scss';

interface CreateUserPropsInterface {
    userStoreService: UserStoreService;
    match: {
        params: {
            id: string;
        }
    }
}

type Props = RouteComponentProps<{}> & CreateUserPropsInterface;

class UserEdit extends Component<Props> {

    public state: UserInterface = {
        _id: '',
        username: '',
        firstName: '',
        lastName: '',
        role: 'user',
        password: '',
        errors: {
            username: false,
            lastName: false,
            firstName: false,
            password: false,
        },
        userAlreadyExist: false
    };

    public componentDidMount() {
        this.userLoaded();
    }
    private userLoaded() {
        const props = this.props;
        props.userStoreService
        .getUser(props.match.params.id)
            .then(
                (data: UserInterface) => {
                    this.setState({
                        _id: data._id,
                        username: data.username,
                        firstName: data.firstName,
                        lastName: data.lastName,
                        role: data.role
                    });
                }
            );
    }

    public render() {
        const props = this.props;
        const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
            const name = event.target.name;
            this.setState({
                [name]: event.target.value
            });
        }

        const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            const regexForUsername = /^[a-zA-Z0-9]{4,16}$/g;
            const regexForFirstName = /^[a-zA-Z]{2,14}$/g;
            const regexForLastName = /^[a-zA-Z]{2,14}$/g;
            const resultRegexForUsername = regexForUsername.test(this.state.username);
            const resultregexForFirstName = regexForFirstName.test(this.state.firstName);
            const resultregexForLastName = regexForLastName.test(this.state.lastName);
            let errors: UserErrorInterface = Object.assign({}, this.state.errors);
            if (!resultRegexForUsername) {
                errors.username = true;
                this.setState({ errors });
            } else {
                errors.username = false;
                this.setState({ errors });
            }
            if (!resultregexForFirstName) {
                errors.firstName = true;
                this.setState({ errors });
            } else {
                errors.firstName = false;
                this.setState({ errors });
            }
            if (!resultregexForLastName) {
                errors.lastName = true;
                this.setState({ errors });
            } else {
                errors.lastName = false;
                this.setState({ errors });
            }
            if ((resultRegexForUsername
                && resultregexForFirstName
                && resultregexForLastName)) {
                props.userStoreService.changeUser(props.match.params.id, this.state)
                    .then(
                        (data: any) => {
                            if (data.statusCode !== 500) {
                                const props = this.props;
                                if (!!props.history) {
                                    props.history.goBack();
                                }
                            } else {
                                this.setState({
                                    userAlreadyExist: true
                                });
                            }
                        }
                    );
            }
        }
        return (
            <Fragment>
                {(!!this.state) ? (
                    <div className="container flex-column">
                        <h2>Edit User</h2>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">Username: </label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="username" type="text" value={this.state.username} onChange={handleChange} />
                                    {(this.state.errors && this.state.errors.username) ? (
                                        <span className="error">Username is required and must have min 4, max 16 characters</span>
                                    ) : null}
                                    {(this.state.userAlreadyExist) ? (<div className="error">User already exist</div>) : null}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">First Name:</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="firstName" type="text" value={this.state.firstName} onChange={handleChange} />
                                    {(this.state.errors && this.state.errors.firstName) ? (
                                        <span className="error">First Name is required and must have min 2, max 14 latin alphabet characters</span>
                                    ) : null}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">Last Name:</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="lastName" type="text" value={this.state.lastName} onChange={handleChange} />
                                    {(this.state.errors && this.state.errors.lastName) ? (
                                        <span className="error">Last Name is required and must have min 2, max 14 latin alphabet characters</span>
                                    ) : null}
                                </div>
                            </div>
                            <label className="col-sm-2 col-form-label col-form-label-sm">Role</label>
                            <select className="custom-select col-sm-10" name="role" onChange={handleChange} value={this.state.role}>
                                <option value='user'>User</option>
                                <option value='admin'>Admin</option>
                            </select>
                            <div className="btn-send">
                                <input className="btn btn-primary mt-2 w-25" type="submit" value="Submit" />
                            </div>
                        </form>
                    </div>
                ) : (<div>Wait plz</div>)}
            </Fragment>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {}
}

export default wrapUserStoreService()(connect(mapStateToProps, DispatchMapper)(UserEdit));