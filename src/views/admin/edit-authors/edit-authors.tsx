//Vendors
import React, { Component } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
//Interfaces
import { AuthorInterface } from '../../../interfaces/author.interface';
//Services
import BookStoreService from "../../../services/book-store.service";
//Actions
import { DispatchMapper, DispatchProps } from '../../../stores/books/book.action';
import { BooksStoreState } from "../../../stores/books";
import { ApplicationState } from "../../..";

interface AuthorPropsInterface {
    bookStoreService: BookStoreService
}

type Props = RouteComponentProps<{}> & AuthorPropsInterface & BooksStoreState & DispatchProps;

type State = {
    authors: string[],
    newAuthor: string,
    error: boolean
}

class AuthorsEdit extends Component<Props, State> {

    public state = {
        authors: [],
        newAuthor: '',
        error: false
    };

    public componentDidMount() {
        this.getAuthors();
    }

    public getAuthors(): void {
        const props = this.props;
        props.authorsLoaded()
    }

    public handleSubmit(event: React.ChangeEvent<HTMLInputElement>): void {
        event.preventDefault();
    }

    public render() {
        const props = this.props;
        const handleSubmit = (event: React.ChangeEvent<HTMLFormElement>): void => {
            event.preventDefault();
            const regexForLastName = /^[a-zA-Z0-9]{2,32}$/g;
            const resultRegexForUsername = regexForLastName.test(this.state.newAuthor);
            if (resultRegexForUsername) {
                props.addNewAuthor(this.state.newAuthor)
                this.setState({
                    newAuthor: ''
                });
                this.setState({
                    error: false
                });
            } else {
                this.setState({
                    error: true
                });
            }
        }

        const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
            this.setState({
                newAuthor: event.target.value
            });
        }

        const deleteAuthor = (id: string): void => {
            props.deleteAuthor(id);
        }

        return (
            <div className="container flex-column">
                <form className="form-inline" onSubmit={handleSubmit}>
                    <div className="form-group mx-sm-3 mb-2">
                        <label className="sr-only">Input author name:</label>
                        <input className="form-control" name="author" type="text" value={this.state.newAuthor} onChange={handleChange} />
                        <button className="btn btn-success" type="submit">Add new Author</button>
                    </div>
                </form>
                {(this.state.error) ? (<div className="error">You mast input atleast 2 latin and no more then 32 characters</div>) : null}
                <h3>All authors:</h3>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {(!!props.authors && props.authors.length) ?
                            props.authors.map((data: AuthorInterface) => {
                                return (
                                    <tr key={data._id}>
                                        <td>{data.name}</td>
                                        <td><button onClick={() => {
                                            deleteAuthor(data._id);
                                        }}>Delete</button></td>
                                    </tr>
                                );
                            }
                            ) : null}
                    </tbody>
                </table>

            </div>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {
        authors: state.book.authors
    }
}

export default (connect(mapStateToProps, DispatchMapper)(AuthorsEdit));