//Vendors
import React, { Component } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from 'react-router-dom';
import Picky, { PickyValue } from 'react-picky';
//Services
import wrapBookStoreService from "../../hoc/wrap-book-store.service";
import BookStoreService from "../../../services/book-store.service";
import { BooksStoreState } from "../../../stores/books";
//Interfaces
import { BookInterface, AuthorInterface, BookErrorInterface } from '../../../interfaces';
//Actions
import { DispatchMapper, DispatchProps } from '../../../stores/books/book.action';
import { ApplicationState } from '../../../index';
//Styles
import 'react-picky/dist/picky.css';

interface AuthorPropsInterface {
    bookStoreService: BookStoreService,
    match: {
        params: {
            id: string;
        }
    };
    history: History;

}

type State = {
    book: BookInterface,
    selected: string[],
    allAuthors: string[],
    errors: {
        name: false,
        img: false,
        description: false,
        authors: false
    }
}

type Props = RouteComponentProps<{}> & AuthorPropsInterface & BooksStoreState & DispatchProps;

class BookEdit extends Component<Props> {

    public state: State = {
        book: {
            _id: '',
            name: '',
            description: '',
            img: '',
            type: 'book',
            author: []
        },
        selected: [],
        allAuthors: [],
        errors: {
            name: false,
            img: false,
            description: false,
            authors: false
        }
    };

    public componentDidMount() {
        const props: Props = this.props;
        if (props.match.params.id !== 'create-book') {
            props.bookStoreService.getBook(props.match.params.id)
                .then(
                    (data: BookInterface) => {
                        props.bookStoreService.getAuthors()
                            .then((authors: AuthorInterface[]) => {
                                this.setState({ allAuthors: [...authors] });
                                let tempArr: string[] = [];
                                data.author.forEach((item: AuthorInterface) => {
                                    tempArr.push(...this.state.allAuthors.filter((el: any) => el.name === item.name));
                                });
                                this.setState({
                                    selected: [...tempArr]
                                });
                            });
                        this.setState({
                            book: {
                                name: data.name,
                                description: data.description,
                                img: data.img,
                                author: data.author,
                                type: data.type
                            },
                            selected: data.author
                        });
                        props.bookLoaded(data);
                    }
                );
        } else {
            props.bookStoreService.getAuthors()
                .then((authors: AuthorInterface[]) => {
                    this.setState({ allAuthors: [...authors] });
                    this.setState({
                        selected: []
                    });
                });
        }
    }

    public render() {
        const props: Props = this.props;
        const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
            const name: string = event.target.name;
            let book: any = Object.assign({}, this.state.book);
            book[name] = event.target.value;
            this.setState({ book });
        }
        const handleChangeFile = (event: React.ChangeEvent<HTMLInputElement>) => {
            let reader = new FileReader();
            let file: any;
            if (event.target.files) {
            file = event.target.files[0];
            }
            reader.readAsDataURL(file);
            reader.onload = () => {
                let book: BookInterface = Object.assign({}, this.state.book);
                if (reader.result) {
                    book.img = reader.result as string;
                }
                this.setState({ book });
            };

        }

        const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            const updateDataForBody = { ...this.state };
            const tempArr: string[] = [];
            updateDataForBody.selected.forEach((item: string) => {
                tempArr.push(item);
            });
            /**
             * VALIDATION
             */
            updateDataForBody.book.author = tempArr as never[];
            const regexForName = /^[a-zA-Z0-9\s]{4,32}$/g;
            const regexForDescription = /^.{4,256}$/g;
            const resultRegexForName = regexForName.test(updateDataForBody.book.name);
            const resultregexForDescription = regexForDescription.test(updateDataForBody.book.description);
            let errors: BookErrorInterface = Object.assign({}, this.state.errors);
            if (!resultRegexForName) {
                errors.name = true;
                this.setState({ errors });
            } else {
                errors.name = false;
                this.setState({ errors });
            }
            if (!resultregexForDescription) {
                errors.description = true;
                this.setState({ errors });
            } else {
                errors.description = false;
                this.setState({ errors });
            }
            if (!updateDataForBody.book.author.length) {
                errors.authors = true;
                this.setState({ errors });
            } else {
                errors.authors = false;
                this.setState({ errors });
            }
            if (!updateDataForBody.book.img) {
                errors.img = true;
                this.setState({ errors });
            } else {
                errors.img = false;
                this.setState({ errors });
            }
            if (updateDataForBody.book.author.length
                && updateDataForBody.book.img
                && resultRegexForName
                && resultregexForDescription) {
                if (props.match.params.id !== 'create-book') {
                    props.changeBook(props.match.params.id, updateDataForBody.book, props.history)
                } else {
                    props.createNewBook(updateDataForBody.book, props.history)
                }
            } else {
                new Error();
            }
        }
        return (
            <div className="edit-books container flex-column">
                {(!!this.state.book) ? (
                    <form onSubmit={handleSubmit}>
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label col-form-label-sm">Title:</label>
                            <div className="col-sm-10 mb-2">
                                <input className="form-control" name="name" type="text" value={this.state.book.name} onChange={handleChange} />
                                {(this.state.errors.name) ? (
                                    <span className="error">Title is required field and must have min 4 and max 32 charters</span>
                                ) : null}
                            </div>
                            <label className="col-sm-2 col-form-label col-form-label-sm">Load image:</label>
                            <div className="col-sm-10 mb-2">
                                <input className="form-control" name="img" type="file" onChange={handleChangeFile} />
                                {(this.state.errors.img) ? (
                                    <span className="error">Image is required field</span>
                                ) : null}
                            </div>
                            <label className="col-sm-2 col-form-label col-form-label-sm">Description:</label>
                            <div className="col-sm-10 mb-2">
                                <textarea className="form-control" name="description" value={this.state.book.description} onChange={handleChange} ></textarea>
                                {(this.state.errors.description) ? (
                                    <span className="error">Description is required field and must have not less than 4 charters and no more 256</span>
                                ) : null}
                            </div>
                            <label className="col-sm-2 col-form-label col-form-label-sm">Select authors:</label>
                            <div className="col-sm-10 mb-2">
                                <Picky
                                    options={this.state.allAuthors}
                                    value={this.state.selected}
                                    multiple={true}
                                    includeSelectAll={false}
                                    includeFilter={true}
                                    valueKey="_id"
                                    labelKey="name"
                                    className="authors-select"
                                    onChange={(values: PickyValue) => {
                                        this.setState({
                                            selected: values
                                        });
                                    }}
                                    dropdownHeight={600}
                                />
                            </div>
                            {(this.state.errors.authors) ? (
                                <span className="error">Authors is required select and must have at least 1 author</span>
                            ) : null}
                            <div className="d-flex w-100 justify-content-end pr-3">
                                <input className="btn btn-secondary mt-2 w-25 mr-2" type="button" value="Back" onClick={() => {
                                    props.historyinputBack(); 
                                }} />
                                <input className="btn btn-primary mt-2 w-25" type="submit" value="Submit" />
                            </div>
                        </div>
                    </form>

                ) : (<div>Wait plz</div>)}
            </div>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {
        book: state.book.book
    }
}

export default wrapBookStoreService()(connect(mapStateToProps, DispatchMapper)(BookEdit));