//Vendors
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Link, RouteComponentProps } from 'react-router-dom';
//Services
import wrapUserStoreService from "../../hoc/wrap-user-store.service";
import UserStoreService from '../../../services/user-store.service';
//Interfaces
import { UserErrorInterface, UserInterface } from '../../../interfaces';
//Actions
import { DispatchMapper } from "../../../stores/users";
import { ApplicationState } from "../../..";

interface CreateUserPropsInterface {
    userStoreService: UserStoreService;
}

type Props = RouteComponentProps<{}> & CreateUserPropsInterface;

class CreateUser extends Component<Props> {

    public state: UserInterface = {
        _id: '',
        username: '',
        firstName: '',
        lastName: '',
        role: 'user',
        password: '',
        errors: {
            username: false,
            lastName: false,
            firstName: false,
            password: false,
        },
        userAlreadyExist: false,
    };

    public componentDidMount() { }

    public render() {
        const props = this.props;
        const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
            const name = event.target.name;
            this.setState({
                [name]: event.target.value
            });
        }
        const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            const regexForUsername = /^[a-zA-Z0-9]{4,16}$/g;
            const regexForPassword = /^.{6,18}$/g;
            const regexForFirstName = /^[a-zA-Z]{2,14}$/g;
            const regexForLastName = /^[a-zA-Z]{2,14}$/g;
            const resultRegexForUsername = regexForUsername.test(this.state.username);
            const resultregexForFirstName = regexForFirstName.test(this.state.firstName);
            const resultregexForLastName = regexForLastName.test(this.state.lastName);
            const resultregexForPassword = regexForPassword.test(this.state.password);
            let errors: UserErrorInterface = Object.assign({}, this.state.errors);
            if (!resultRegexForUsername) {
                errors.username = true;
                this.setState({ errors });
            } else {
                errors.username = false;
                this.setState({ errors });
            }
            if (!resultregexForFirstName) {
                errors.firstName = true;
                this.setState({ errors });
            } else {
                errors.firstName = false;
                this.setState({ errors });
            }
            if (!resultregexForLastName) {
                errors.lastName = true;
                this.setState({ errors });
            } else {
                errors.lastName = false;
                this.setState({ errors });
            }
            if (!resultregexForPassword) {
                errors.password = true;
                this.setState({ errors });
            } else {
                errors.password = false;
                this.setState({ errors });
            }
            if (resultRegexForUsername
                && resultregexForFirstName
                && resultregexForLastName
                && resultregexForPassword) {
                props.userStoreService.createNewUser(this.state)
                    .then(
                        (data) => {
                            const props = this.props;
                            if (!!props.history && data.status === 201) {
                                props.history.go(-1);
                            } else if (data.status !== 201) {
                                this.setState({
                                    userAlreadyExist: true
                                });
                            }
                        }
                    );
            }
        }
        return (
            <Fragment>
                {(!!this.state) ? (
                    <div className="container flex-column">
                        <h2>Register</h2>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">Username: </label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="username" type="text" value={this.state.username} onChange={handleChange} />
                                    {(this.state.errors && this.state.errors.username) ? (
                                        <span className="error">Username is required and must have min 4, max 16 characters</span>
                                    ) : null}
                                    {(this.state.userAlreadyExist) ? (<div className="error">User already exist</div>) : null}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">First Name:</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="firstName" type="text" value={this.state.firstName} onChange={handleChange} />
                                    {(this.state.errors && this.state.errors.firstName) ? (
                                        <span className="error">First Name is required and must have min 2, max 14 latin alphabet characters</span>
                                    ) : null}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">Last Name:</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="lastName" type="text" value={this.state.lastName} onChange={handleChange} />
                                    {(this.state.errors && this.state.errors.lastName) ? (
                                        <span className="error">Last Name is required and must have min 2, max 14 latin alphabet characters</span>
                                    ) : null}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label col-form-label-sm">Password: </label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="password" type="password" value={this.state.password} onChange={handleChange} />
                                    {(this.state.errors && this.state.errors.password) ? (
                                        <span className="error">Password is required and must have min 6, max 18 characters</span>
                                    ) : null}
                                </div>
                            </div>
                            <label className="col-sm-2 col-form-label col-form-label-sm">Role</label>
                            <select className="custom-select col-sm-10" name="role" onChange={handleChange} value={this.state.role}>
                                <option value='user'>User</option>
                                <option value='admin'>Admin</option>
                            </select>
                            <div className="btn-send">
                                {(props.location.pathname === '/register') ? (
                                    <Link className="mr-2" to="/login" >Login</Link>
                                ) : null}
                                <input className="btn btn-primary mt-2 w-25" type="submit" value="Submit" />
                            </div>
                        </form>
                    </div>
                ) : (<div>Wait plz</div>)}
            </Fragment>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {
        // editUser: state.user.editUser
    }
}

export default wrapUserStoreService()(connect(mapStateToProps, DispatchMapper)(CreateUser));