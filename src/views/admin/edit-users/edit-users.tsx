//Vendors
import React, { Component } from "react";
import { connect } from "react-redux";
import ReactPaginate from 'react-paginate';
import { Link, RouteComponentProps } from 'react-router-dom';
//Services
import UserStoreService from "../../../services/user-store.service";
import wrapUserStoreService from "../../hoc/wrap-user-store.service";
//Interfaces
import { CountInterface, UserInterface } from "../../../interfaces";
//Actions
import { DispatchMapper, DispatchProps } from "../../../stores/users/user.action";
import { ApplicationState } from '../../../index';
//Styles
import './edit-users.css';


interface CreateUserPropsInterface {
    userStoreService: UserStoreService;
    match: {
        params: {
            id: string;
        }
    }
}

type State = {
    users: UserInterface[],
    totalCountItem: CountInterface,
    currentPage: number
}

type Props = RouteComponentProps<{}> & CreateUserPropsInterface & DispatchProps;

class UsersEdit extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            users: [],
            totalCountItem: {
                totalCountUsers: 0,
                totalCountBooks: 0
            },
            currentPage: 1
        }
    }


    public componentDidMount() {
        const props = this.props;
        this.getUsers();
        props.userStoreService.getCountsInfo()
            .then((data: CountInterface[]) => {
                this.setState({
                    totalCountItem: data[0]
                });
            });
    }
    private getUsers(page?: number, limit?: number) {
        const props = this.props;
        props.userStoreService.getUsers(page, limit)
            .then(
                (users: UserInterface[]) => {
                    this.setState({
                        users: users
                    })
                    props.usersLoaded(users);
                }
            )
    }
    public render() {
        const props = this.props;
        const deleteUser = (id: string, onPage?: number) => {
            props.userStoreService.deleteUser(id)
                .then(
                    () => {
                        this.getUsers(onPage);
                        let totalCountItem: CountInterface = Object.assign({}, this.state.totalCountItem);
                        totalCountItem.totalCountUsers = totalCountItem.totalCountUsers - 1;
                        this.setState({
                            totalCountItem
                        });
                        props.usersLoaded(this.state.users);
                    }
                );
        }

        return (
            <div className="users container flex-column">
                <ReactPaginate
                    pageCount={((this.state.totalCountItem.totalCountUsers) / 12)}
                    pageRangeDisplayed={5}
                    marginPagesDisplayed={2}
                    previousLabel={'previous'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-class'}
                    onPageChange={(selectedItem: { selected: number }) => {
                        this.getUsers(selectedItem.selected + 1);
                        this.setState({
                            currentPage: (selectedItem.selected + 1)
                        });
                    }}
                    initialPage={0}
                    forcePage={0}
                    disableInitialCallback={false}
                    containerClassName={'container'}
                    pageClassName={'page-li'}
                    pageLinkClassName={'page-a'}
                    activeClassName={'active'}
                    previousClassName={'previous-li'}
                    nextClassName={'next-li'}
                    previousLinkClassName={'previous-a'}
                    nextLinkClassName={'next-a'}
                    disabledClassName={'disabled'}
                    hrefBuilder={(pageIndex: number) => null}
                    extraAriaContext={'aria'}
                />
                <Link className="btn btn-success" to={`users/create-user`}>Add new User</Link>
                <table className="table table-condensed">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Role</th>
                            <th>Edit/Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {(!!this.state.users && this.state.users.length) ?
                            this.state.users.map((data: UserInterface, index: number) => {
                                return (
                                    <tr key={data._id}>
                                        <td>{data.username}</td>
                                        <td>{data.firstName}</td>
                                        <td>{data.lastName}</td>
                                        <td>{data.role}</td>
                                        <td><Link className="text-primary" to={`users/${data._id}`}>Edit</Link>
                                            <span className="ml-1 mr-2"> &frasl;</span>
                                            <button className="text-danger" onClick={() => {
                                                if (data._id) {
                                                    deleteUser(data._id, this.state.currentPage);
                                                }
                                            }}>Delete</button>
                                        </td>
                                    </tr>
                                );
                            }
                            ) : null}
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {
        users: state.user.users
    }
}

export default wrapUserStoreService()(connect(mapStateToProps, DispatchMapper)(UsersEdit));