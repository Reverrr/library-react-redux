//Vendors
import React, { Fragment } from "react";
import { Route, Switch, NavLink } from 'react-router-dom';
//Components
import UsersEdit from './edit-users/edit-users';
import BooksEdit from './edit-books/edit-books';
import BookEdit from './edit-book/edit-book';
import AuthorsEdit from './edit-authors/edit-authors';
import UserEdit from './edit-user/edit-user';
import CreateUser from './create-user/create-user';
//Styles
import './admin.scss';
const Admin = () => {
    return (
        <Fragment>
            <div className="container">
                <div className="column">
                    <nav className="navbar navbar-light nav-admin-edit">
                        <NavLink activeClassName="active" to='/admin/users' className="option-admin btn btn-outline-primary mr-2">Edit Users</NavLink >
                        <NavLink activeClassName="active" to='/admin/books' className="option-admin btn btn-outline-primary mr-2">Edit Library</NavLink >
                        <NavLink activeClassName="active" to='/admin/authors' className="option-admin btn btn-outline-primary mr-2">Edit Authors</NavLink >
                    </nav>
                </div>
            </div>
            <Switch>
                <Route path='/admin/users' component={UsersEdit} exact />
                <Route path='/admin/users/create-user' component={CreateUser} exact />
                <Route path='/admin/users/:id' component={UserEdit} exact />
                <Route path='/admin/books' component={BooksEdit} exact />
                <Route path='/admin/books/edit-book/:id' component={BookEdit} />
                <Route path='/admin/books/:id' component={BookEdit} />
                <Route path='/admin/authors' component={AuthorsEdit} />
            </Switch>
        </Fragment>
    );
}

export default Admin;