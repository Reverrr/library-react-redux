//Vendors
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, RouteComponentProps } from 'react-router-dom';
import ReactPaginate from 'react-paginate';
//Services
import wrapBookStoreService from "../../hoc/wrap-book-store.service";
import BookStoreService from "../../../services/book-store.service";
//Interfaces
import { CountInterface } from "../../../interfaces";
import { AuthorInterface } from '../../../interfaces/author.interface';
import { BookInterface } from '../../../interfaces/book.interface';
//Actions
import { DispatchMapper, DispatchProps } from '../../../stores/books/book.action';
import { BooksStoreState } from "../../../stores/books";
import { ApplicationState } from "../../..";

interface CreateBookPropsInterface {
    bookStoreService: BookStoreService;
}

type Props = RouteComponentProps<{}> & CreateBookPropsInterface & BooksStoreState & DispatchProps;

type State = {
    books: BookInterface[],
    totalCountItem: CountInterface,
    currentPage: number
}

class EditBooks extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            books: [],
            totalCountItem: {
                totalCountBooks: 0,
                totalCountUsers: 0
            },
            currentPage: 1
        }
    }

    public componentDidMount() {
        const props = this.props;
        this.loadBooks();
        props.bookStoreService.getCountsInfo()
            .then((value: CountInterface[]) => {
                this.setState({
                    totalCountItem: value[0]
                });
            });
    }

    private loadBooks(page: number = 1) {
        const props = this.props;
        props.booksLoaded('', [], page)
    }

    public render() {
        const props = this.props;
        const deleteBook = async (id: string, index: number) => {
            await props.deleteBook(id, this.state.currentPage + 1)
        }
        return (
            <div className="edit-books container flex-column">
                <ReactPaginate
                    pageCount={(this.state.totalCountItem.totalCountBooks / 9)}
                    pageRangeDisplayed={5}
                    marginPagesDisplayed={2}
                    previousLabel={'previous'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-class'}
                    onPageChange={(selectedItem: { selected: number }) => {
                        this.setState({
                            currentPage: selectedItem.selected
                        })
                        this.loadBooks(selectedItem.selected + 1);
                    }}
                    initialPage={0}
                    forcePage={0}
                    disableInitialCallback={false}
                    containerClassName={'container'}
                    pageClassName={'page-li'}
                    pageLinkClassName={'page-a'}
                    activeClassName={'active'}
                    previousClassName={'previous-li'}
                    nextClassName={'next-li'}
                    previousLinkClassName={'previous-a'}
                    nextLinkClassName={'next-a'}
                    disabledClassName={'disabled'}
                    hrefBuilder={(pageIndex: number) => null}
                    extraAriaContext={'aria'}
                />
                <Link className="btn btn-success" to={`/admin/books/create-book/`}>Add new book</Link>
                <table className="table table-condensed">
                    <thead>
                        <tr>
                            <th className="column-width">Title</th>
                            <th className="column-width">description</th>
                            <th className="column-width">Authors</th>
                            <th className="column-width">Edit/Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {(!!props.books && props.books.length) ?
                            props.books.map((data: BookInterface, index: number) => {
                                return (
                                    <tr key={data._id}>
                                        <td className="column-width">{data.name}</td>
                                        <td className="column-width">{data.description}</td>
                                        <td className="column-width">
                                            Authors: {data.author.map((item: AuthorInterface) => {
                                                return (<span key={item._id}>{item.name}</span>)
                                            })}
                                        </td>
                                        <td className="column-width">
                                            <Link className="text-primary" to={`/admin/books/edit-book/${data._id}`}>Edit</Link>
                                            <span className="ml-1 mr-2"> &frasl;</span>
                                            <button className="text-danger" onClick={() => {
                                                if (data._id) {
                                                    deleteBook(data._id, index);
                                                }
                                            }}>Delete</button>
                                        </td>
                                    </tr>
                                );
                            }
                            ) : null}
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {
        books: state.book.books
    }
}

export default wrapBookStoreService()(connect(mapStateToProps, DispatchMapper)(EditBooks));
