//Vendors
import React, { Component } from "react";
import { connect } from "react-redux";
import BookListItem from "../book-list-item/book-list-item";
import ReactPaginate from 'react-paginate';
import Picky from 'react-picky';
import { RouteComponentProps } from 'react-router-dom';
//Services
import wrapBookStoreService from "../hoc/wrap-book-store.service";
import BookStoreService from "../../services/book-store.service";
//Interfaces
import { CountInterface, AuthorInterface } from "../../interfaces";
import { BookInterface } from '../../interfaces/book.interface';
//Actions
import { BooksStoreState, DispatchMapper, DispatchProps } from "../../stores/books";
import { ApplicationState } from '../../index';
//Styles
import 'react-picky/dist/picky.css';
import './book-list.scss';

interface BookListPropsInterface {
    bookStoreService: BookStoreService,
    match: {
        params: {
            id: string;
        }
    };
    history: History;
}

export type Props = RouteComponentProps<{}> & BooksStoreState & DispatchProps & BookListPropsInterface;

export type State = {
    totalCountItem: {
        totalCountBooks: number,
        totalCountUsers: number
    },
    searchStatus: boolean,
    valueSearch: string,
    allAuthors: AuthorInterface[],
    selected: string[],
    selectedName: string[]
}

class BookList extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            totalCountItem: {
                totalCountBooks: 0,
                totalCountUsers: 0
            },
            searchStatus: false,
            valueSearch: '',
            allAuthors: [],
            selected: [],
            selectedName: []
        }
    }

    private loadBooks(request: string = '', authors: string[] = [], page: number = 1, type: string = '' , count: number = 9) {
        const props = this.props;
        props.booksLoaded(request, authors, page, type, count)
    }

    public componentDidMount() {
        this.loadBooks();
        const props = this.props;
        props.bookStoreService.getCountsInfo()
            .then((data: CountInterface[]) => {
                this.setState({
                    totalCountItem: data[0]
                });
            });
        props.bookStoreService.getAuthors()
            .then((authors: AuthorInterface[]) => {
                this.setState({ allAuthors: [...authors] });
            });
    }

    public render() {
        const props = this.props;
        const searchEvent = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
            (event.target.value === '' && !this.state.selected.length)
                ? this.setState({
                    searchStatus: false
                }) : this.setState({
                    searchStatus: true
                })
            this.setState({
                valueSearch: event.target.value
            });
            this.loadBooks(event.target.value, this.state.selectedName);
        }
        return (
            <div className="wrap-books">
                <div className="books">
                    <div className="pagination">
                        {(!this.state.searchStatus) ? (
                            <ReactPaginate
                                pageCount={(this.state.totalCountItem.totalCountBooks / 9)}
                                pageRangeDisplayed={5}
                                marginPagesDisplayed={2}
                                previousLabel={'previous'}
                                nextLabel={'next'}
                                breakLabel={'...'}
                                breakClassName={'break-class'}
                                onPageChange={(selectedItem: { selected: number }) => {
                                    this.loadBooks(this.state.valueSearch, this.state.selectedName, (selectedItem.selected + 1));
                                }}
                                initialPage={0}
                                forcePage={0}
                                disableInitialCallback={false}
                                containerClassName={'container'}
                                pageClassName={'page-li'}
                                pageLinkClassName={'page-a'}
                                activeClassName={'active'}
                                previousClassName={'previous-li'}
                                nextClassName={'next-li'}
                                previousLinkClassName={'previous-a'}
                                nextLinkClassName={'next-a'}
                                disabledClassName={'disabled'}
                                hrefBuilder={(pageIndex: number) => null}
                                extraAriaContext={'aria'}
                            />
                        ) : null}
                    </div>
                    <div className="books-container">
                        {
                            (props.books) ?
                                props.books.map((book: BookInterface) => {
                                    return (
                                        <div className="book" key={book._id}><BookListItem book={book} /></div>
                                    );
                                }) : null
                        }
                    </div>
                </div>
                <div className="main-filters">
                    <div className="form-group item-serach md-form mt-0">
                        <input className="form-control item-serach" type="text" onChange={searchEvent} placeholder="Search" aria-label="Search" maxLength={16} />
                    </div>
                    <Picky
                        options={this.state.allAuthors}
                        value={this.state.selected}
                        multiple={true}
                        includeSelectAll={false}
                        includeFilter={true}
                        valueKey="_id"
                        labelKey="name"
                        onChange={(values: any) => {
                            const valuesName: any = [];
                            values.forEach((item: any) => {
                                valuesName.push(item.name);
                            });
                            this.setState({
                                selected: values,
                                selectedName: valuesName
                            });
                            (valuesName.length || this.state.valueSearch)
                                ? this.setState({
                                    searchStatus: true
                                })
                                : this.setState({
                                    searchStatus: false
                                })
                            this.loadBooks(this.state.valueSearch, valuesName);
                        }}
                        dropdownHeight={600}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {
        books: state.book.books
    }
}
//BookList
export default wrapBookStoreService()(connect(mapStateToProps, DispatchMapper)(BookList));