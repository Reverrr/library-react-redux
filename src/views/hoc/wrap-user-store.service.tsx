//Vendors
import React, { ReactNode } from 'react';
import { UserStoreServiceConsumer } from '../user-store-service-context/user-store-service-context';
import UserStoreService from '../../services/user-store.service';
import AuthStoreService from '../../services/auth.service';

const authStoreService = new AuthStoreService();

const wrapUserStoreService = () => (Wrapped: any) => {
    return (props: any) => {
        return (
            <UserStoreServiceConsumer>
                {
                    (userStoreService: UserStoreService) => {
                        return <Wrapped {...props} userStoreService={userStoreService} authStoreService={authStoreService} />
                    }
                }
            </UserStoreServiceConsumer>
        );
    }
}


export default wrapUserStoreService;