//Vendors
import React, { ReactNode } from 'react';
import { BookStoreServiceConsumer } from '../book-store-service-context/book-store-service-context';
import BookStoreService from '../../services/book-store.service';

const wrapBookStoreService = () => (Wrapped: any) => {
    return (props: any) => {
        return (
            <BookStoreServiceConsumer>
                {
                    (bookStoreService: BookStoreService) => {
                        return <Wrapped {...props} bookStoreService={bookStoreService} />
                    }
                }
            </BookStoreServiceConsumer>
        );
    }
}


export default wrapBookStoreService;