//Interfaces
import {
    UserAccessInterface,
} from '../interfaces';
//Environment
import { environment } from '../react-app-env';

export default class AuthStoreService {

    public login(body: {username: string, password: string}): Promise<UserAccessInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/auth/login`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public auth(token: string): Promise<UserAccessInterface> {
        (token) ? token : token = ' ';
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/auth/verify`, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            }).then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }
}