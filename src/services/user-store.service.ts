//Interfaces
import {
    UserInterface,
    PasswordInterface,
    CountInterface
} from '../interfaces';
//Environment
import { environment } from '../react-app-env';

export default class UserStoreService {

    public getUsers(page: number = 1, limit: number = 12): Promise<UserInterface[]> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/users?page=${page}&_limit=${limit}`, { method: 'GET' })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public getUser(id: string): Promise<UserInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/users/${id}`, { method: 'GET' })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public changeUser(id: string, body: UserInterface): Promise<UserInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/users/${id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public createNewUser(body: UserInterface) {
        delete body._id;
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/users/`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public deleteUser(id: string): Promise<UserInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/users/${id}`, { method: 'DELETE' })
                .then((response: any) => {
                    res(response)
                })
        }).then((data: any) => { return data.json(); });
    }

    public changePassword(body: PasswordInterface): Promise<PasswordInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/users/changeUserPassword`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public getCountsInfo(): Promise<CountInterface[]> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/count`, { method: 'GET' })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }
}