//Interfaces
import {
    BookInterface,
    AuthorInterface,
    CountInterface
} from '../interfaces';
//Environment
import { environment } from '../react-app-env';

export default class BookStoreService {

    public getBooks<BookInterface>(): Promise<BookInterface[]> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/books`, { method: 'GET' })
                .then((response: any) => { return res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public getСhunkBooks(request: string = '', count: number = 9, authors: string[] = [], type: string = '', page: number = 1): Promise<BookInterface[]> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/books?q=${request}&_limit=${count}&authors=${authors}&type=${type}&page=${page}`, { method: 'GET' })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public getBook(id: string): Promise<BookInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/books/${id}`, { method: 'GET' })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public getBookById(id: string): Promise<BookInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/books/${id}`, { method: 'GET' })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public createNewBook(body: BookInterface): Promise<BookInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/books/`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            })
                .then((response: any) => { res(response) })
        });
    }

    public changeBook(id: string, body: BookInterface): Promise<BookInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/books/${id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public getAuthors(): Promise<AuthorInterface[]> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/authors`, { method: 'GET' })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public deleteAuthor(id: string): Promise<AuthorInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/authors/${id}`, { method: 'DELETE' })
                .then((response: any) => {
                    res(response)
                })
        }).then((data: any) => { return data.json(); });
    }

    public deleteBook(id: string): Promise<BookInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/books/${id}`, { method: 'DELETE' })
                .then((response: any) => {
                    res(response)
                })
        }).then((data: any) => { return data.json(); });
    }

    public addNewAuthor(name: string): Promise<AuthorInterface> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/authors`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ name: name })
            })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }

    public getCountsInfo(): Promise<CountInterface[]> {
        return new Promise((res, rej) => {
            fetch(`${environment.nodeUrl}/count`, { method: 'GET' })
                .then((response: any) => { res(response) })
        }).then((data: any) => { return data.json(); });
    }
}