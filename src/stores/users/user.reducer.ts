//Vendors
import { UserInterface } from '../../interfaces/user.interface';
//Actions
import { KnownAction } from './user.action';
import { Reducer, AnyAction } from 'redux';

export interface UsersStoreState {
    users: UserInterface[];
    currentUser: UserInterface;
    token: string;
}

export const unloadedState: UsersStoreState = {
    users: [],
    currentUser: {
        username: '',
        firstName: '',
        lastName: '',
        role: '',
        password: '',       
    },
    token: ''
};

type test = UsersStoreState | undefined;

export const userReducer: Reducer<UsersStoreState, AnyAction> = (stateUser: UsersStoreState | any = {}, incomingAction: AnyAction) => {
    const state = stateUser as UsersStoreState;
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'USERS_LOADED': {
            return {
                ...state,
                users: action.payload
            };
        }

        case 'USER_LOADED':
            return {
                ...state,
                editUser: action.payload
            };

        case 'CURRENT_USER':
            return {
                ...state,
                currentUser: action.payload
            };

        case 'SELECTED_USER': {
            return {
                ...state,
                selectedUserEdit: action.payload
            };
        }
        case 'SET_TOKEN': {
            return {
                ...state,
                token: action.payload.newToken
            };
        }
        default: {
            return state || unloadedState;
        }
    }
}

export default userReducer;