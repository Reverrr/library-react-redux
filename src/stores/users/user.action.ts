//Vendors
import { Dispatch } from "react";
//Interfaces
import { UserInterface, UserAccessInterface } from "../../interfaces";
//Services
import UserStoreService from '../../services/user-store.service';

let userStoreService = new UserStoreService();

interface UsersLoadedAction {
    type: 'USERS_LOADED';
    payload: UserInterface[]
}
interface UserLoadedAction {
	type: 'USER_LOADED';
	payload: UserInterface;
}
interface DeleteUserAction {
	type: 'DELETE_USER';
	payload: UserInterface;
}
interface CurrentUserAction {
	type: 'CURRENT_USER';
	payload: UserInterface;
}
interface SetTokenAction {
	type: 'SET_TOKEN';
	payload: UserAccessInterface;
}
interface SelectedUser {
	type: 'SELECTED_USER';
	payload: UserInterface;
}

export type KnownAction = UsersLoadedAction | UserLoadedAction | DeleteUserAction | CurrentUserAction | SetTokenAction |SelectedUser;

export interface DispatchProps {
	usersLoaded(newUsers: UserInterface[]): void;
	userLoaded(newUser: UserInterface): void;
    deleteUser(newUsers: UserInterface): void;
    currentUser(newUser: UserInterface): void;
	selectedUser(id: string): void;
    setToken(newUsers: UserAccessInterface): void; 
}


export const DispatchMapper = (dispatch: Dispatch<KnownAction>): DispatchProps => {
    
    return {
        selectedUser: (id: string) => {
            userStoreService.getUser(id)
            .then((data) => {
                dispatch({ type: "SELECTED_USER", payload: data })
            })
        },
        usersLoaded: (newUsers: UserInterface[]) => {
            dispatch({ type: "USERS_LOADED", payload: newUsers })
        },
        userLoaded: (newUser: UserInterface)=> {
            dispatch({ type: "USER_LOADED", payload: newUser })
        },
        deleteUser: (newUsers: UserInterface) => {
            dispatch({ type: "DELETE_USER", payload: newUsers })
        },
        currentUser: (newUser: UserInterface) => {
            dispatch({ type: "CURRENT_USER", payload: newUser })
        },
        setToken: (newUser: UserAccessInterface) => {
            dispatch({ type: "SET_TOKEN", payload: newUser })
        }
	};
}