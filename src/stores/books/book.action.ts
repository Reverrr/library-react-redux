//Vendors
import { Dispatch } from 'react';
//Interfaces
import { BookInterface, AuthorInterface, CountInterface } from '../../interfaces';
//Services
import BookStoreService from '../../services/book-store.service';

let bookStoreService = new BookStoreService();


interface BooksLoadedAction {
    type: 'BOOKS_LOADED';
    payload: BookInterface[]
}
interface BookLoadedAction {
	type: 'BOOK_LOADED';
	payload: BookInterface;
}
interface DeleteBookAction {
	type: 'BOOK_DELETE';
	payload: BookInterface;
}
interface CurrentBookAction {
	type: 'AUTHORS_LOADED';
	payload: AuthorInterface[];
}

interface TotalCount {
	type: 'TOTAL_COUNTS';
	payload: CountInterface;
}

export type KnownAction = BooksLoadedAction | BookLoadedAction | DeleteBookAction | CurrentBookAction | TotalCount;

export interface DispatchProps {
    addNewAuthor(name: string): void;
    deleteAuthor(id: string): void;
	bookLoaded(newBook: BookInterface): void;
	getBook(id: string): void;
    deleteBook(id: string,currentPage: number): void;
    authorsLoaded(): void;
    createNewBook(data: BookInterface, history: History): void;
    booksLoaded(request: string, authors: string[], page: number, type?: string, count?: number): void;
    changeBook(id: string, book: BookInterface, history: History): void;
}

export const DispatchMapper = (dispatch: Dispatch<KnownAction>): DispatchProps => {

    const authorsLoaded = () => {
        bookStoreService.getAuthors()
        .then((authors) => {
            dispatch({ type: "AUTHORS_LOADED", payload: authors })
        })
    }
    const booksLoaded = (currentPage: number) => {
        /**
         * Load books for pagination
         */
        bookStoreService.getСhunkBooks('', 9, [], '', currentPage )
        .then((books) => {
            dispatch({ type: "BOOKS_LOADED", payload: books })
        });
    }

	return {
        createNewBook: (data: BookInterface, history: History) => {
            bookStoreService.createNewBook(data)
            .then(() => {
                history.go(-1);
            });
        },
        changeBook: (id: string, book: BookInterface, history: History) => {
            bookStoreService.changeBook(id, book)
            .then(
                (data: BookInterface) => {
                    history.go(-1);
                }
            );           
        },
        addNewAuthor: (name: string) => {
            bookStoreService.addNewAuthor(name).then(() => {
                authorsLoaded();
            });
        },
        deleteAuthor: (id: string) => {
            bookStoreService.deleteAuthor(id)
            .then(() => {
                authorsLoaded();
            });
        },
        bookLoaded: (newBook: BookInterface)=> { 
            dispatch({ type: "BOOK_LOADED", payload: newBook })
        },
        getBook: (id: string)=> {
            bookStoreService.getBookById(id)
            .then((newBook) => {
                dispatch({ type: "BOOK_LOADED", payload: newBook })
            })
        },
        deleteBook: (id: string, currentPage: number)=> {
            bookStoreService.deleteBook(id)
            .then((book) => {
                booksLoaded(currentPage)
                dispatch({ type: "BOOK_DELETE", payload: book })
               
            })
        },
        authorsLoaded: () => {
            bookStoreService.getAuthors()
            .then((authors) => {
                dispatch({ type: "AUTHORS_LOADED", payload: authors })
            })
        },
        booksLoaded: (request?: string, authors?: string[], page?: number, type?: string, count?: number) => {
            bookStoreService.getСhunkBooks(request, count, authors, type, page )
            .then((books) => {
                dispatch({ type: "BOOKS_LOADED", payload: books })
            });
        }
	};
}
