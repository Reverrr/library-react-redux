//Vendors
import { BookInterface } from '../../interfaces/book.interface';
import { AuthorInterface } from '../../interfaces/author.interface';
//Actions
import { KnownAction } from './book.action';
//Interfaces
import { CountInterface } from '../../interfaces/count.interface';
import { Reducer, AnyAction } from 'redux';

export interface BooksStoreState {
    books: BookInterface[];
    book: BookInterface;
    totalCount: CountInterface;
    authors: AuthorInterface[];
}

export const bookReducer: Reducer<BooksStoreState, AnyAction> = (stateBook: BooksStoreState | any = {}, incomingAction: AnyAction) => {
    const state = stateBook as BooksStoreState;
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'BOOKS_LOADED': {
            return {
                ...state,
                books: action.payload
            };
        }
        case 'BOOK_LOADED': {
            return {
                ...state,
                book: action.payload
            };
        }

        case 'AUTHORS_LOADED': {

            return {
                ...state,
                authors: action.payload
            };
        }

        case 'TOTAL_COUNTS': {
            return {
                ...state,
                totalCount: action.payload
            };
        }

        default: {
            return state;
        }
    }
}

export default bookReducer;